
Here, the USNB has been extended for supporting an Online Social Learning Network Service (OSLN), so that trainees may experiment concrete online participation (e.g., discussion together via the  USNB) in relation with their training. Here, the online social learning network is a type of social network that results from interaction between learners, teachers, and modules of learning. 
In addition, increasing participant engagement (i.e., course completion time) has been individually investigated in this work. A recommendation system has been designed and implemented to offer appropriate study partners to learners. Offering appropriate study partners can encourage learners to be more active and stay in the courses. Last but not least, getting benefit from the user interactive environment provided by the USNB, the learners can communicate to the recommended study partners through their social network messengers as well as the course forum.

Edx is a well-known MOOC provider in the world. In this work, we use open edX. Open edX is an open-source platform that powers edX courses. It is freely available. Using Open edX , educators and technologists can build learning tools and contribute new features to the platform, creating innovative solutions to benefit learners everywhere (for more information about OpenedX, please see the following link: About Us).



We consider and implement a use case where a learner has registered in two MOOC courses (i.e., Java and Network courses). The learner has a Facebook account (or Slack) and prefers to : (1) do exercise of his courses, and/or (2) access to discussion forum of his courses, and/or (3) find and communicate to study partner(s) using his Facebook Messenger (or Slack).

In first step, the learner selects "MOOC" in the menu to get a list of his enrolled courses.
Technically speaking, we need to get information of courses (i.e., a list of enrolled courses) for the learner from MOOC platform. To this end, we first run the following API request (see, https://github.com/edx/edx-platform/blob/master/common/djangoapps/enrollment/views.py):

GET /api/enrollment/v1/enrollment/{username}
If the request is successful, an HTTP 200 "OK" answer is returned. The HTTP 200 answer includes several values such as course and user details.

We implemented getting information (i.e., course list) from MOOC via USNB in both Facebook Messenger and Slack environment.

After finding and choosing the appropriate enrolled course in the list, as already mentioned, the learner may have access to exercise or forum of the selected course. He may also ask the recommendation system to offer appropriate study partners. 

## Access to forum ##
In order to have access to an online course forum information, we use the following APIs considering GET and POST methods (see, https://openedx.atlassian.net/wiki/spaces/EDUCATOR/pages/26183643/Discussion+API):

 "/api/discussion/v1/threads/?course$\_$id=selectedcourse" for threads. 

And "/api/discussion/v1/comments/?course$\_$id=selectedcourse" for comments.

Here, GET retrieves a list of threads and POST creates a new thread. Figure~\ref{GETP} shows more details of them.

## Access to exercise ##
If the learner clicks on the course exercise, he can access to the selected course exercise. 

Technically speaking, to have access to exercise of the course, we first need to introduce concept of xBlock. A Course structure is a hierarchical Recursive Composition of primitive and composite objects known as xBlocks. An xBlock is the basic building block of any edX course. "Problem" or "exercise" is one example of xBlock type.

With the above viewing enhancements for an xBlock, there are now various choices for viewing an xBlock within the context of a course. These comprehensive choices are provided to consumers of the Course Blocks API for their discretionary use (see https://openedx.atlassian.net/wiki/spaces/EDUCATOR/pages/29688043/Course+Blocks+API). We use "student$\_$view$\_$url" to get url of an exercises.


## Implementing recommendation system ##
Consider the learner asks "Study Partners". 

Here, we describe how to implement a generalized version of the recommendation system where it finds and recommends study partners based on a principle feature (i.e., learners similarity feature). After implementing the general version of the recommendation system, adding other features to the recommendation system will be easily possible. 
For each student (i.e., learner), the system first creates a document. The document includes the learner text (e.g., extracted from course forum, social networks). After that, using LDA, the topics are extracted. The similarity among topics of different documents (i.e., learners) is measured using Cosine technique. 
And, finally, it recommends the most similar study partner to each learner.
We implemented the recommendation system in both Facebook messenger and Slack.

