//
//
//
//
//
'use strict';

var request = require("request");

var botconfig = require('../conf/bot.conf');

// to send direct msg to slack
const {WebClient} = require('@slack/client');
const web = new WebClient(botconfig.token);

module.exports = {
  processSendMeButtons: processSendMeButtons, // makes main through generateButtonsHtml and send it to slack
  preparetosendMessageToSlack : preparetosendMessageToSlack, // it will be used in amqp endpoint (receiver)
  generateButtonsHtml : generateButtonsHtml, // makes the main menu 
  sendWebChatAttacheements: sendWebChatAttacheements,  //
  webChatPostMessage: webChatPostMessage, //Ahvar: added to be exported
sendMessageToSlackResponseURL:sendMessageToSlackResponseURL //ahvar was added for MOOC
};

//
//

//it takes a list to make buttoms based on that
function processSendMeButtons(destinationUrl,arraynames, texttoshow) {
  try {
	  //ahvar: updated. because we changed generateButtonsHtml.
  var message = {
    "text": "",
    "attachments": [    ]
  };
 	message.attachments.push(generateButtonsHtml(arraynames,texttoshow));
	//console.log(message);
    sendMessageToSlackResponseURL(destinationUrl, message).then()
  } 
  catch (e) {
	//  console.log('error in making the menu and send to slack');
  }
}
//
//
function sendWebChatAttacheements(channel) {
  var attachments = [];
  generateButtonsHtml(["About","Users","MOOC"],texttoshow);
  web.chat.postMessage({
    channel: channel,
    "attachments": [generateButtonsHtml(["About","Users","MOOC"],texttoshow)]
  });
}

//
//
function webChatPostMessage(msgToSend) {
  web.chat.postMessage(msgToSend)
    .then((res) => {
      // `res` contains information about the posted message
      console.log('Message sent: ', res.ts);
    })
    .catch(console.error);
}



// we use it in receiver when it consumes from RabitMQ and wants to send the obtained msg to slack
//
function preparetosendMessageToSlack(message) {

  //    switch (message.getTo().name) {
  switch (message._type.toLowerCase()) {

    case "about":
      // msg processing should be added here//
      web.chat.postMessage({
        channel: message.getTo().channelid,

        "attachments": [
          {
            // "text": "Choose a user",
            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "MainMenu",
            "author_name": message._message.text,
            //"image_url": "https://drive.google.com/open?id=1DE1CtGbBS363WMmFv-ib8Br66XL1OVEp",
            "image_url": "https://preview.ibb.co/jtM978/twitter_292988_1920.jpg",
            "actions": [
              {
                "name": "MainMenu",
                "text": "Main Menu",
                "type": "button",
                "value": "MainMenu"
              },
              {
                "name": "VisitPages",
                "text": "Visit Pages",
                "type": "button",
                "value": "VisitPages"
              },
            ]
          }
        ]
      })
        .then((res) => {
          // `res` contains information about the posted message
          console.log('Message sent: ', res.ts);
        })
        .catch(console.error);
      break;

    case "ls":
      // PGR NOTE: why print to console only for this case ?
      // also, when the message is outmouted, a small info before to give the context /method
      // where the log is displayed is important to help understand the console output
      // debug messages should be consistent and explicit
      console.log(message);

      var options = [];
      var photos = [":checkered_flag: ", ":heart: ", ":bridge_at_night: ", " :girl: ", " :frog: ", ":ghost: "]

      for (var i = 0; i < message._message.length; i++) {
        var option = {
          "text": photos[i%(photos.length)] + message._message[i].userId + ' : ' + message._message[i].about,
          "value": message._message[i].userId,

        }

        options.push(option);

      }

      var message2slack = {
        "text": "",
        "response_type": "in_channel",
        "attachments": [
          {
            "text": "Choose a user",
            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "user_selection",
            "actions": [

              {
                "name": "users_list",
                "text": "Pick a user...",
                "type": "select",
                "options": options
              },
              {
                "name": "MainMenu",
                "text": "Main Menu",
                "type": "button",
                "value": "MainMenu"
              },
            ]

          }
        ]
      }


      sendMessageToSlackResponseURL(message.getTo().url, message2slack)
      break;
    case "to":
      //in BUS uniqueName is channelid
      web.chat.postMessage({
        channel: message.getTo().uniqueName,
        text: "(" + message.getFrom().name + ") : " + message._message
      })
        .then((res) => {
          // `res` contains information about the posted message
          console.log('Message sent: ', res.ts);
        })
        .catch(console.error);
      break;


  }
}

// creates the buttons for main menu


//makes buttons based on a arraynames
function generateButtonsHtml(arraynames,texttoshow) {

    var attachments = [];
      var options = [];

      for (var i = 0; i < arraynames.length; i++)
      {

        var option = {
            "text": arraynames[i],
            "value": arraynames[i],
            "name": arraynames[i],
            "type": "button",	    
		}

    	options.push(option);

      }

var trigger=Math.random();
    attachments=
      {
        "text": texttoshow,
        "fallback": "Shame... buttons aren't supported in this land",
        "callback_id": "button_tutorial",
        "color": "#3AA3E3",
        "attachment_type": "default",
       "trigger_id": trigger*100,
        "actions": options
      };
//console.log(attachments);
  return attachments;
}


//
// private methods
//




// We use it in slack action post.
function sendMessageToSlackResponseURL(responseURL, JSONmessage) {

  const postOptions = {
    uri: responseURL,
    method: "POST",
    headers: {
      "Content-type": "application/json"
    },
    json: JSONmessage
  };
  request(postOptions, (error, response, body) => {
    if (error) {
      // handle errors as you see fit
      console.log(JSON.stringify(error));
      return false;
    }
    return true;
  });
}
