// 22 dec the list of users can be taken automatically through rest API   
//const maxecercise=3; // fb has  limitation to show number of exercises we set it 3 here to be same as fb for demo 


//To do: 27 Nov , auth that we used in whole text and also sometimes username should be replaced automatically 
//5 dec to do : replace sho with actionJSONPayload.user.name in the APIs
//
//6 dec: topic id should be saved, now I put all the questions in general topic, so I know the topic id, otherwise it has a issue  
//
'use strict';
const lda = require('./llda-master/lib/lda');
var similarity = require( 'compute-cosine-similarity' );

const maxecercise=3; // fb has  limitation to show number of exercises we set it 3 here to be same as fb for demo 

var users=[];
users=["Ani-Mani","Shohreh-Ahvar","Ehsan-Ahvar","Rafael-Angarita"]; 

var HashMap = require('hashmap');
const axios = require('axios');
const apiUrl = 'https://slack.com/api';
const qs = require('querystring');
// configs
var botconfig = require('./conf/bot.conf');
var serverconfig = require('./conf/server.conf');
var moocconfig = require('./conf/MOOC.conf');

var http = require('http');
var express = require('express');
var request = require('request');
var request2 = require('request-promise');
var bodyParser = require('body-parser');
// PGR NOTE: here we have a variable that is a copy of a config info. Not necessary, can use serverconfig.port
// everywhere insteadd. Much cleaner to understand where the info is stored and avoids bad usage. Ahvar : done.
// Needed to send msg to amqp
//Ahvar : we moved sender up here.
var sender = require('./amqp-sender');

// They are needed to add interactive messages and buttons
var Message = require('message-transformer/message');


const slackManager = require('./managers/slackCommand');


// PGR NOTE: here we have a global variable used by an API method and rtm.
// this should be avoided as it will generate issues down the line.
// global variables should be avoided as much as possible.
var userState = new HashMap();
var ThreadUsers=new HashMap();
var CourseUsers=new HashMap();
var DocUsers=new HashMap();
var CommentUsers=new HashMap();
var QUsers=new HashMap();
var CourseIDname=new HashMap();
//
// Create and launch server
//

console.log('Create server and listen on port ' + serverconfig.port);
var app = express();
var urlencodedParser = bodyParser.urlencoded({extended: false});
http.createServer(app).listen(serverconfig.port);



// RealTimeMessaging 

const {RTMClient} = require('@slack/client');
// PGR NOTE: variable names should be long/meaningful. Here I ave no clue what rtm means.
// this is important for code readability

//
const rtm = new RTMClient(botconfig.token);
rtm.start();
var eventtext = '';
rtm.on('message', (event) => {
  // For structure of `event`, see https://api.slack.com/events/message

  if (eventtext != event.text && event.subtype != 'bot_message') {
 //   console.log(event);
 //   console.log(`(channel:${event.channel}) ${event.user} says: ${event.text}`);

    //    var timeOfPostback = event.timestamp;

    // PGR NOTE: beware bellow: javascript has = == and === as well as != !==
    // below should be !==
    if (userState.get(event.channel) !== undefined) {
      if (event.text !== undefined && event.text.toLowerCase() === "quit") {

        // PGR NOTE : below doesnt make sense. why build a message (which is entirely hardcoded)
        // and then only use one subpart. Bad coding should be fixed
//        var message = slackManager.generateButtonsHtml();
//        var attachments = message.attachments;
//        // message.text='You ended the conversation with ' + userState.get(event.channel) + '.';
//        web.chat.postMessage({
//          channel: event.channel,
//          "attachments": attachments
//        });
        slackManager.sendWebChatAttacheements(event.channel);

        userState.delete(event.channel);

      } else {

        //sendTextMessage(senderID, 'Message sent to ' + userState.get(senderID) + '!');
        var message = new Message();
        message._from = {
          name: event.user,
          uniqueName: event.channel
        };
        message._to = [userState.get(event.channel)];
        message._message = event.text; //should be taken from user
        message._type = 'TO';
        //            message._persona = confamqp.exchange.name;
        message._persona = 'slack'; // to inform bus that the messag is from slack

        sender.post(message, 'usnb');

        //       }
      }
    }
  }
  eventtext = event.text;
});


//Ahvar: we dont use 2 following functions, so we removed them
//sendMessageToSlack with out URL and channel ID , we dont use it here

/*

function sendMessageToSlackwithChannelID(JSONmessage, ChannelID) {
  var postOptions = {
    channel: ChannelID,
    uri: 'https://slack.com/api/chat.postMessage',
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    json: JSONmessage
  }
  request(postOptions, (error, response, body) => {
    if (error) {
      console.log(" problem " + error);

    }
  })
}

function sendMessageToSlack(JSONmessage) {
  var postOptions = {
    //uri: responseURL,
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    json: JSONmessage
  }
  request(postOptions, (error, response, body) => {
    if (error) {
      // handle errors as you see fit
    }
  })
}

*/



//
// REAT API methods for server
//

//
//
app.post('/slack/action', urlencodedParser, (req, res) => {
//console.log(req);
//console.log(res);
  res.status(200).end(); // best practice to respond with 200 status
  var actionJSONPayload = JSON.parse(req.body.payload); // parse URL-encoded payload JSON string
if(actionJSONPayload.type=="interactive_message")
  switch (actionJSONPayload.actions[0].name) {



    case "VisitPages":
      /*visit website should be added */
      break;
    case "MainMenu":
      // PGR NOTE what is the difference between this case and the slash-commands/send-me-buttons ??
      //Ahvar : they are the same. In both case we will show the main menu
      // var message = MainMenu();
      // sendMessageToSlackResponseURL(actionJSONPayload.response_url, message)
      slackManager.processSendMeButtons(actionJSONPayload.response_url,["About","Users","MOOC"], "What do you want to do?");
      break;

    case "About":
      // build about msg
      var message = new Message();
      message._from = {
        name: 'About',
        url: actionJSONPayload.response_url,
        channelid: actionJSONPayload.channel.id,
        uniqueName: actionJSONPayload.user.name
      };
      message._message = "About";
      message._type = "COMMAND";
      message._persona = 'slack';

      sender.post(message, 'usnb');      // send to RabbiMQ

      break;
    case "Users":
      // build users message
      var message = new Message();
      message._from = {
        name: 'ls',
        url: actionJSONPayload.response_url,
        channelid: actionJSONPayload.channel.id,
        uniqueName: actionJSONPayload.user.name
      };
      message._message = "ls";
      message._type = "COMMAND";
      message._persona = 'slack';

      sender.post(message, 'usnb'); // 'usnb' is exchange value which shows msg goes to bus
      break;
    case "users_list": // when users click on the menu

      var senderID = actionJSONPayload.channel.id;
      var recipientID = actionJSONPayload.actions[0].selected_options[0].value;

      //       console.log(actionJSONPayload.actions[0].selected_options[0].value);
      userState.set(senderID, recipientID);
      //sendStartSeparator(senderID, "This is a conversation with " + recipientID + ". Write \"quit\" to end it.");

      // PGR NOTE : Moved in slackCommad manager - message still build here but may make sense
      // to only send senderID and recipientID ?

      //web.chat.postMessage({
      //  channel: senderID,
      //  "attachments": [
      //    {
      //      "author_name": "",
      //      "image_url": "https://drive.google.com/open?id=1DE1CtGbBS363WMmFv-ib8Br66XL1OVEp",
      //      text: "This is a conversation with " + recipientID + ". Write \"quit\" to end it."
      //    }
      //  ]
      //})
      //  .then((res) => {
      //    // `res` contains information about the posted message
      //    console.log('Message sent: ', res.ts);
      //  })
      //  .catch(console.error);
      var msgToSend = {
        channel: senderID,
        "attachments": [
          {
            "author_name": "",
            "image_url": "https://drive.google.com/open?id=1DE1CtGbBS363WMmFv-ib8Br66XL1OVEp",
            text: "This is a conversation with " + recipientID + ". Write \"quit\" to end it."
          }
        ]
      };
      slackManager.webChatPostMessage(msgToSend);


      break;
    case "Course_list": // when users click on one of the courses in the course list 

 //console.log("injas");

      var SelectedCourse = actionJSONPayload.actions[0].selected_options[0].value;
      CourseUsers.set(actionJSONPayload.user.name,SelectedCourse);
//"course-v1:edX+DemoX+Demo_Course

///api/discussion/v1/courses/\(courseID)
//https://github.com/edx/edx-app-ios/blob/ea5a634595155ee46ba7ec7b2c10e1cc0b62cb44/Source/DiscussionAPI.swift
//var cours="course-v1:edX+DemoX+Demo_Course";

//course_id":"course-v1:edX+DemoX+Demo_Course

//request.get((moocconfig.URLREST)+'/api/discussion/v1/course_topics/"+cours, {


//13 Nov : I chenged requested_fields=graded,format,student_view_multi_device,lti_url,due    to requested_fields=student_view_multi_device,due
//due is for the name of the problem, I should checck is it correct or not ot it is sth else 



//27 Nov there was an issue that when we search a block of course name, if there is + in the name, it should be replaced with %2B, this is duty of this while
//var selectcourseforprint=SelectedCourse;
while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
{
SelectedCourse=SelectedCourse.replace('+','%2B');
//console.log(SelectedCourse);
}



  slackManager.processSendMeButtons(actionJSONPayload.response_url, ["Exercise","Forum","MainMenu", "Study Partners"], "You are in course "+CourseIDname.get(actionJSONPayload.actions[0].selected_options[0].value)+". What you want to do?");

//request.get((moocconfig.URLREST)+'/xblock/block-v1:edX+DemoX+Demo_Course+type@problem+block@1e2cd6ce16144eb9a39979f472e96148", {
//request.get((moocconfig.URLREST)+'/api/courses/v1/blocks/?course_id=course-v1:edX%2BDemoX%2BDemo_Course&username=sho2&&depth=all&requested_fields=student_view_multi_device&block_counts=problem&student_view_data=problem&block_types_filter=problem",{


      break;

case "Threads_list":
  ThreadUsers.set(actionJSONPayload.user.name,actionJSONPayload.actions[0].selected_options[0].value);
      var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);

//4 dec to do : show the text of question , or even answers .
  slackManager.processSendMeButtons(actionJSONPayload.response_url, ["Question","Answer","MainMenu"],"You are in the forum for course "+CourseIDname.get(SelectedCourse)+". What you want to do?");

break;
case "Question":
var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);

const { text:textQ, trigger_id:trigger_idQ , response_url:response_urlQ,token:tokenQ} = actionJSONPayload;
Dial(textQ, trigger_idQ, response_urlQ,tokenQ,"Question");  //////   ??????

break;

case "Exercise":

var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);

while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
{
SelectedCourse=SelectedCourse.replace('+','%2B');

}


var link=(moocconfig.URLREST)+"/api/courses/v1/blocks/?username=sho2&&depth=all&requested_fields=student_view_multi_device&block_counts=problem&student_view_data=problem&block_types_filter=problem&course_id="+SelectedCourse;
//console.log(link);
request.get(link,{
//course-v1:edX+DemoX+Demo_Course
//username=sho2&&depth=all&requested_fields=student_view_multi_device&block_counts=problem&student_view_data=problem&block_types_filter=problem&course_id="+SelectedCourse,{
// 27 Nov to do : username should come from inputs in the APIs
//display_name
//student_view_url
  'auth': {
    'bearer': moocconfig.token
  }
}, function (error, response, body) {

//console.log(body);
/////20 Nov
body=JSON.parse(body);
//console.log(Object.keys(body.blocks).length);
//console.log(body.blocks[Object.keys(body.blocks)[0]].student_view_url);
      var options = [];

      for (var i = 0    ; i < Object.keys(body.blocks).length && i<maxecercise; i++) {
if(body.blocks[Object.keys(body.blocks)[i]].display_name!="")
{
        var option = {
                "name": body.blocks[Object.keys(body.blocks)[i]].display_name,
                "text": body.blocks[Object.keys(body.blocks)[i]].display_name,
            "type": "button",
        "url": body.blocks[Object.keys(body.blocks)[i]].student_view_url,

            "value": body.blocks[Object.keys(body.blocks)[i]].display_name

        }
}
else
{
var option = {
                "name": 'Exercise',
                "text": 'Exercise'+i,  //check hete 
            "type": "button",
        "url": body.blocks[Object.keys(body.blocks)[i]].student_view_url,

            "value": 'Exercise'
    }


}

        options.push(option);

      }

var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);
/////20 Nov
      var message2slack = {
        "text": "",
        "response_type": "in_channel",
        "attachments": [
          {
            "text": "You are in the exercise section for course "+CourseIDname.get(SelectedCourse)+". What you want to do?",
            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "exercise_selection",
            "actions": options  // since we wanted to redirect user to a link if the special exercie is selected, we had t use bottomn, because with list is not possible
           

          },

        ]
      }
      slackManager.sendMessageToSlackResponseURL(actionJSONPayload.response_url, message2slack);


}); //end of excersise API




break;

case "Forum":


var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);

while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
{
SelectedCourse=SelectedCourse.replace('+','%2B');

}

var threadlink=(moocconfig.URLREST)+"/api/discussion/v1/threads/?course_id="+SelectedCourse;
//console.log(link);
request.get(threadlink,{
  'auth': {
    'bearer': moocconfig.token
  }
}, function (error, response, body) {


body=JSON.parse(body);
//console.log(body.results);
//console.log(body.results[0].title);
      var threadsoptions = [];

for (var i = 0; i < body.results.length; i++) {

if(body.results[i].title!="")
{
        var option = {
                "text": body.results[i].title,
                "value": body.results[i].id,  // to use it later in the case //to do 27 NOV
        }
}
        threadsoptions.push(option);
}


//body.results[i].title
//threadsoption extraction 27 nov


var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);
/////20 Nov
      var message2slack = {
        "text": "",
        "response_type": "in_channel",
        "attachments": [

          {
             "text": "Here is the thread list for course "+CourseIDname.get(SelectedCourse)+". Pick a thread",

            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "Forum access",
            "actions": 
             [ {
                "name": "Threads_list",
                "text": "Choose a Topic from Forum",
                "type": "select",
                "options": threadsoptions
              },,
              {
                "name": "MainMenu",
                "text": "Main Menu",
                "type": "button",
                "value": "MainMenu"
              },]


          }
        ]
      }
      slackManager.sendMessageToSlackResponseURL(actionJSONPayload.response_url, message2slack);

});//end of thread API





break;

case "Study Partners":

      var options = [];
      var photos = [":checkered_flag: ", ":heart: ", ":bridge_at_night: ", " :girl: ", " :frog: ", ":ghost: "]
    var SelectedCourse=CourseUsers.get(actionJSONPayload.user.name);

    while((SelectedCourse.localeCompare(SelectedCourse.replace('+','%2B')))!=0)
    {
    SelectedCourse=SelectedCourse.replace('+','%2B');

    }
      var collection=[];
//      collection=recom(actionJSONPayload.user.name, SelectedCourse,users,collection,"comments",recom(actionJSONPayload.user.name, SelectedCourse,collection,users,collection,"threads"));
//      collection=recom(actionJSONPayload.user.name, SelectedCourse,users,collection,"comments");collection=recom(actionJSONPayload.user.name, SelectedCourse,collection,users,collection,"threads"));
   //   var message=recom(actionJSONPayload.user.name, SelectedCourse,users,collection);
      var message=[];
recom('Ani-Mani', SelectedCourse,users).then(function(message){


//console.log('message'+message);
//      var message=similarity(collection,'sho',);  //to change sho 6 dec 
      for (var i = 0; i < message.length; i++) {
        var option = {
//          "text": photos[i%(photos.length)] + message._message[i].userId + ' : ' + message._message[i].about,
//          "value": message._message[i].userId,
          "text": photos[i%(photos.length)] + message[i],
          "value": message[i],
        }

        options.push(option);

      }

      var message2slack = {
        "text": "",
        "response_type": "in_channel",
        "attachments": [
          {
            "text": "Choose a user",
            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "user_selection",
            "actions": [

              {
                "name": "users_list",
                "text": "Pick a user...",
                "type": "select",
                "options": options
              },
              {
                "name": "MainMenu",
                "text": "Main Menu",
                "type": "button",
                "value": "MainMenu"
              },
            ]

          }
        ]
      }


      slackManager.sendMessageToSlackResponseURL(actionJSONPayload.response_url, message2slack)

})
.catch(function (error){});


      break;



break;


case "Answer":

const { text, trigger_id , response_url,token} = actionJSONPayload;
    Dial(text, trigger_id, response_url,token,"Answer");







break;
case "ShowThread":
//??
break;



    case "MOOC":


request.get((moocconfig.URLREST)+'/api/enrollment/v1/enrollment?', {
  'auth': {
    'bearer': moocconfig.token
  }
}, function (error, response, body) {
// embed the enrolled courses in a menu  
body=JSON.parse(body);
//console.log(body);
//console.log(body[0].course_details.course_id);
      var options = [];

      for (var i = 0; i < body.length; i++)
      {

        var option = {
            "text": body[i].course_details.course_name,
            "value": body[i].course_details.course_id

        }

        options.push(option);
            CourseIDname.set(body[i].course_details.course_id,body[i].course_details.course_name);   

      }

      var message2slack = {
        "text": "",
        "response_type": "in_channel",
        "attachments": [
          {
            "text": "Choose a course from your enrolled courses",
            "fallback": "If you could read this message, you'd be choosing something fun to do right now.",
            "color": "#3AA3E3",
            "attachment_type": "default",
            "callback_id": "course_selection",
            "actions": [

              {
                "name": "Course_list",
                "text": "Pick a course...",
                "type": "select",
                "options": options
              },
 
              {
                "name": "MainMenu",
                "text": "Main Menu",
                "type": "button",
                "value": "MainMenu"
              },
            ]

          }
        ]
      }

//send the msg to slack 

      slackManager.sendMessageToSlackResponseURL(actionJSONPayload.response_url, message2slack);
});
break;




  };

  if (actionJSONPayload.type=="dialog_submission") {
switch (actionJSONPayload.callback_id){
case "Question":

//console.log(actionJSONPayload);
//actionJSONPayload.user.name // in social bus I think is based on channelID ,we can change it ther based on user.name

//var API="http://(moocconfig.URLREST)+'/api/discussion/v1/comments/?thread_id="+ThreadUsers(actionJSONPayload.user.name);
var API=(moocconfig.URLREST)+"/api/discussion/v1/threads/?thread_id="+ThreadUsers.get(actionJSONPayload.user.name);
//console.log(CourseUsers.get(actionJSONPayload.user.name));
//console.log(API);
var post_param2={title:actionJSONPayload.submission.title,raw_body:actionJSONPayload.submission.description,type:actionJSONPayload.submission.Type,course_id:CourseUsers.get(actionJSONPayload.user.name), topic_id: 'edx_demo_embedded_discussion'}; ///to do topic id of each course should be saved in one map  )};

//console.log(post_param2);
request.post(API,{ form:post_param2, method: 'POST',
   'auth': {
    'bearer': moocconfig.token
  },
}, function (error, response, body) {
//console.log( body);
});


//API explanations : https://github.com/edx/edx-platform/blob/63d84d65e5f8562ef20560a161288512c96b2d9c/lms/djangoapps/discussion_api/views.py


//var API=(moocconfig.URLREST)+'/api/discussion/v1/threads/?thread_id=5be443746e95520a65000000";
//var post_param2={title:"TestQ",raw_body:"test",type:"discussion", topic_id :,course_id: 'course-v1:edX+DemoX+Demo_Course'};
       ///api/discussion/v1/course_topics    
       ///api/discussion/v1/course_topics    
//{ POST /api/discussion/v1/threads/
//  "course_id": "course-v1:TestX+TestCourse+TestRun",
//  "topic_id": "df3e1598d8544e04a34dfcfa22313caf",
//  "type": "discussion",
//  "title": ,
//  "raw_body": "**Example Thread Body**",

break;

case "Answer":



var API=(moocconfig.URLREST)+"/api/discussion/v1/comments/?thread_id="+ThreadUsers.get(actionJSONPayload.user.name);
var post_param2={raw_body:actionJSONPayload.submission.description,thread_id:ThreadUsers.get(actionJSONPayload.user.name)};
//console.log(API);
request.post(API,{ form:post_param2, method: 'POST',
   'auth': {
    'bearer': moocconfig.token
  },
}, function (error, response, body) {
//console.log( body);
});

//})   .then(response => response.json())
//  .then(data => {
//       console.log(data);   });

//curl -v -X POST -H "Content-Type: application/x-www-form-urlencoded;charset=utf-8;text/html" -H "X-CSRFToken: UoWgncyheZTdV19tFytuf6HLVJBUolPT" -d "email=miss.ahoor@gmail.com&password=sho3&remember=false" --cookie "csrftoken=UoWgncyheZTdV19tFytuf6HLVJBUolPT" (moocconfig.URLREST)+'/user_api/v1/account/login_session/  
break;


}//switch callback
//break;
} // if 

});

//
//
app.get('/ping', urlencodedParser, (req, res) => {
  // best practice to respond with 200 status
  res.status(200).end();
});

//
//
app.post('/slack/slash-commands/send-me-buttons', urlencodedParser, (req, res) => {


///MOOC

// obtain the User information including the enrolled courses

//to do : take courses for the user , how we find the username, request.get((moocconfig.URLREST)+'/api/enrollment/v1/enrollment?user=+username', {          var senderID = actionJSONPayload.channel.id;


// To do : logg in 

/*request.get((moocconfig.URLREST)+'/api/enrollment/v1/enrollment?user=ehs', {
  'auth': {
    'bearer': moocconfig.token
  }
}
*/
///

///20 Nov : request('post','/user_api/v1/account/login_session/', user,function (error, response, body){} ); maybe we should call another API in this func
//try /oauth2/access_token/",


/////20 Nov try requests.post('{}/user_api/v1/account/login_session/'.format(base_url),data=data, cookies=cookies, headers=headers, auth=auth)


/////20 Nov



  var reqBody = req.body
  var responseURL = reqBody.response_url
  // test valid parameters

  // call slack manager
  slackManager.processSendMeButtons(responseURL, ["About","Users","MOOC"], "What do you want to do?");

  // best practice to respond with empty 200 status code
  res.status(200).end();
});

// PGR NOTE : why is this method exported by the server ? moved to stackManager
// module.exports.preparetosendMessageToSlack = preparetosendMessageToSlack;

// cherte az inja va hamchinin call kardanesh 


///4 dec
function Dial(text, trigger_id , response_url,token,callbackname) {

switch  (callbackname) {

case "Answer":

var element=[];
element.push(

          {
            label: 'Description',
            type: 'textarea',
            name: 'description',
            optional: false,
          },
);

break;
case "Question":

var element=[];
element.push(
          {
            label: 'Title',
            type: 'text',
            name: 'title',
            value: text,
            hint: '30 second summary of the problem',
          },
          {
            label: 'Description',
            type: 'textarea',
            name: 'description',
            optional: false,
          },
{
            label: 'Type',
            type: 'select',
            name: 'Type',
            options: [
              { label: 'question', value: 'question' },
              { label: 'discussion', value: 'discussion' },

            ]});
break;
}//end of switch callbackname

    const dialog = {
      token: botconfig.token,
      trigger_id,
      dialog: JSON.stringify({
        title: callbackname,
        callback_id: callbackname,
        submit_label: 'Submit',
        elements: element,

      }),
    };

console.log(dialog);
//response_url="https://hooks.slack.com/";
//apiUrl=response_url;

 axios.post(`${apiUrl}/dialog.open`, qs.stringify(dialog))

      .then((result) => {
        debug('dialog.open: %o', result.data);
        res.send('');
      }).catch((err) => {
        debug('dialog.open call failed: %o', err);
        res.sendStatus(500);
});


}



function DialComment(text, trigger_id , response_url,token,callbackname) {



    const dialog = {
      token: botconfig.token,
      trigger_id,
      dialog: JSON.stringify({
        title: 'Submit an Answer',
        callback_id: callbackname,
        submit_label: 'Submit',
        elements: [
          {
            label: 'Title',
            type: 'text',
            name: 'title',
            value: text,
            hint: '30 second summary of the problem',
          },
          {
            label: 'Description',
            type: 'textarea',
            name: 'description',
            optional: true,
          },
{
            label: 'Type',
            type: 'select',
            name: 'Type',
            options: [
              { label: 'question', value: 'question' },
              { label: 'discussion', value: 'discussion' },

            ],
},


        ],
      }),
    };
//response_url="https://hooks.slack.com/";
//apiUrl=response_url;

 axios.post(`${apiUrl}/dialog.open`, qs.stringify(dialog))

      .then((result) => {
        debug('dialog.open: %o', result.data);
        res.send('');
      }).catch((err) => {
        debug('dialog.open call failed: %o', err);
        res.sendStatus(500);
});


}




const recom = async (name, course,users) => {
//async function main() {

var collection=[];

var API=(moocconfig.URLREST)+"/api/discussion/v1/threads/?course_id="+course;  // 5 dec to check, should I give the name here ? 
//console.log(API);
var threadIDs=[];
var threadendores=[];



/////////////here request calling for questions ///////
  let body = await doRequest(API);
//console.log("to make sure 2");
//console.log(body);
                                  var array=[];
                                                      body=JSON.parse(body);
                                                 //     console.log(body.results[0]);
                            
                                if(body.results!=null)
                            for(var i=0; i<body.results.length;i++){ //for1
                                array=[];
                            threadIDs[i]=body.results[i].id;
                            if(body.results[i].type=="question")
                                    threadendores[i]=body.results[i].has_endorsed;
                            else
                                    threadendores[i]=-1;
                             //   console.log("thread "+i+"user: "+body.results[i].author+" withtext : "+body.results[i].raw_body+" users: "+DocUsers.get(body.results[i].author));
                            if(DocUsers.get(body.results[i].author)==null){ // if there is not already in the hash for this user any question recorded 
                             //   console.log("not saved ");
                                array.push(body.results[i].raw_body);}
                                else{
                            //    console.log(DocUsers.get(body.results[i].author));
                                array=DocUsers.get(body.results[i].author);
                                array.push(body.results[i].raw_body);
                            }
                            DocUsers.set(body.results[i].author,array);
                             
                        //         console.log('DocQuestion for this user'+DocUsers.get(body.results[i].author));

                          //  console.log('thread '+i+ ':  ' + threadIDs[i]);

                            //console.log( threadIDs[i]);
                            } //for1

                     //        console.log('   ***********        \n');
                       //10 dec     });  // 






                    //         console.log('   *****comments******        \n');

                                       //            for(var l=0; l<users.length;l++)
                                                   for(var k=0; k<threadIDs.length;k++){//for2


                                    if(threadendores[k]!=-1)
                                    var API2=(moocconfig.URLREST)+"/api/discussion/v1/comments/?course_id="+course+"&endorsed=0&thread_id=";
                                    else
                                    var API2=(moocconfig.URLREST)+"/api/discussion/v1/comments/?course_id="+course+"&thread_id=";
        
                                API2=API2+threadIDs[k];
                                //To do : if type is discussion we should not give endors to it 
                             //       console.log(API2);

  let body = await doRequest(API2);
//console.log(res);

                                                    //console.log( " thread: "+threadIDs[k]);
                                                    //console.log( body);
                                                    //console.log('\n \n thread '+k+ ':  ' + body);
                                                             var array=[];

                                                    body=JSON.parse(body);
                                                    if(body!=null )
                                                    if(body.results!=null )
                                                    {

                                                                               for(var i=0; i<body.results.length;i++){ //for results.length
                                                        array=[];

                                                //        console.log("thread "+k+"user: "+body.results[i].author+" withtext : "+body.results[i].raw_body+" dousers: "+DocUsers.get(body.results[i].author));
                                                        if(DocUsers.get(body.results[i].author)==null){ // if there is not already in the hash for this user any question recorded 
                                                  //          console.log("not saved ");
                                                            array.push(body.results[i].raw_body);}
                                                        else{

                                                            array=DocUsers.get(body.results[i].author);
                                                            array.push(body.results[i].raw_body);
                                                        }
                                                        DocUsers.set(body.results[i].author,array);
                                                  //          console.log(DocUsers.get(body.results[i].author));
                                                       }//for results.length
                                


                                                    }

                             //       });//end req


                                }//for 2   
/// taking the data from Docusers(user) to collection 
//console.log('DocUsers after comment '+DocUsers);

for (var y=0;y<users.length; y++)
{
//collection[y]=[];
if(DocUsers.get(users[y])!=null)
{

 //   console.log('array : '+DocUsers.get(users[y]));
 //   collection.push(DocUsers.get(users[y]));
    collection.push(DocUsers.get(users[y]));
//console.log('after Q phase, Collection for user '+users[y]+' is : '+ collection[y]);
 //   CommentUsers.set(users[y],(DocUsers.get(users[y])).length-(QUsers.get(users[y]).length));
}


}
//console.log(collection);

var users2=[];
users2=users;
var u=users.length;
/// remove the users that has no text in the forum 
for (var t=0,  y=0;y<u; y++)
{
if(DocUsers.get(users[y])!=null)
{	//users.splice(y, 1);

	users[t]=users2[y];
t++;
 //       collection.splice(y, 1);
}

}


//console.log(users);


/// LDA +simillary for study partners
/// LDA
var counterm=0;
var terms=new HashMap();
var probabilities = [];
var partnersimilarity = [];
var partnerID ; // should be array 
var Tresults = [];
var dense = [];
var Totalresult=[];

collection.forEach((documents, i) => {
  const results = lda(documents, 1, 2, null, null, null, 123);  // we will take 1 topic including several terms, the next lines are based on this fact 
console.log('\n ********************** Texts from user '+users[i]+'**********************');
console.log('\n '+documents);
//console.log('\n Topic of user '+users[i]);
//console.log(results);
//Tresults[i].push(results);
  // Save the probabilities for each group. The values should be the same, since we're using the same random seed.

  results.forEach(group => {
    group.forEach(row => {
      if(terms.get(row.term)==null)
      {
	terms.set(row.term,counterm);
	counterm++;
      }
    });
  });

      Totalresult.push(results);

});
   console.log('\n ******************LDA runs finished ***************** \n');
 //  console.log('\n ******************dense vector creation  ***************** \n');
var o=0;


Totalresult.forEach(results => {
    dense[o]=[]; // we initialize the 
    for(var b=0;b<counterm;b++)
	dense[o][b]=0;
 // console.log(dense[o]);
  results.forEach(group => {
    

    group.forEach(row => {


	//      console.log('term is '+row.term+' and the value is '+ terms.get(row.term));
	      if(terms.get(row.term)!=null)
	      {
	
		dense[o][terms.get(row.term)]=row.probability;

	       }
		
	
	

    });

  });
o++;
});


//3 dec To do , just do for the user 
var s;
var partners=[];
for ( var i = 0; i < dense.length; i++ ) {
     partnersimilarity[i]=-1;
     for ( var j = 0; j < dense.length; j++ ) {
         if(i!=j && users[i]==name)
            {
	//	console.log('dense[i]'+dense[i]);
	//	console.log('dense[j]'+dense[j]);
            s = similarity( dense[i], dense[j] );
            console.log('Similarity for' + name + 'and '+ users[j]+'is  '+s); //To do : we should retrive the nam eof student number i 
          if(partnersimilarity[i]<s)  // 
       //   if(partnersimilarity[i]-s<2)  // here we can say if 
          {
          partnersimilarity[i]=s;
        partnerID=(j); //  
          }
         }

             


      }


// 10 dec 5:20 PM here we can merge QUsers and CommentUsers and similarity 

        if (users[i]==name)
{
        partners.push(users[partnerID]);
            console.log('Recommended study partner for'+name+'is student '+users[partnerID]); //To do : we should retrive the nam eof student number i 
}
   }




//console.log(partners);

// partners=await sendback(partners);
return partners;














/// 3 dec : to DO: put chats in the collection array 

//take chat of all students 



}
/////////////////////3 dec





function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request.get(url,{

          'auth': {
            'bearer': moocconfig.token
          },

                }, function (error, res,body) {
//console.log('error of request '+error);
//console.log('body of req'+body);
//console.log("to make sure 1");
      if (!error && res.statusCode == 200) {
        resolve(body);
      } else {
        reject(error);
      }
    });
  });
}

// Usage:
