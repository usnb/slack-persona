/**
 * Parses messages of type
 '#to={id1,.., idn} The message'
 **/

var Message = require('./message.js');

function getMessage(originalMessage) {

    parsedMessage = parse(originalMessage);
    var message = new Message(parsedMessage.to, '', '', parsedMessage.message,
        parsedMessage.type);

    console.log("parser return: " + message.getType());
    return message;
}

function parse(data) {
    data = String(data);
    var result = {};
    if (data != undefined) {

        var destinations = data.match(/#to={.*}/);

        if (destinations != null) {
            //TO message
            var destionationsArray = destinations.toString().match(/{(.*)}/);
            destionationsArray = destionationsArray[1].split(',')
                .map(Function.prototype.call, String.prototype.trim);

            result.to = destionationsArray;
            //remove the destinations from the message, trim it, and remove
            //line breaks from the start and the end
            result.message = data.replace(destinations[0], '')
                .trim()
                .replace(/^\s+|\s+$/g, '');

            result.type = 'TO';
        } else if (data.indexOf('#about') >= 0) { //about command
            result.to = '';
            result.message = 'about';
            result.type = 'COMMAND';
        } else if (data.indexOf('#ls') >= 0) { //ls command
            result.to = '';
            result.message = 'ls';
            /*var att = data.match(/#more(.*)/);
            var c = att[1].trim().split(' ')[0];
            result.attributes = {c};*/
            result.type = 'COMMAND';
        } else if (data.indexOf('#help') >= 0) { //help command
            result.to = '';
            result.message = 'help';
            result.type = 'COMMAND';
        } else if (data.indexOf('#more') >= 0) { //help command
            var result = data.match(/#more(.*)/);
            var user = result[1].trim().split(' ')[0];
            result.to = '';
            result.message = 'more ' + user;
            result.type = 'COMMAND';
        } else if (data.indexOf('#vote') >= 0) { //vote command
            //var result = data.match(/#more(.*)/);
            //var project = result[1].trim().split(' ')[0];
            result.to = '';
            result.message = 'vote';
            result.type = 'COMMAND';
        } else if (data.indexOf('#idea') >= 0) { //idea command
            result.to = '';
            result.message = 'idea';
            result.type = 'COMMAND';
        }  else { //error message
            result.to = '';
            result.message = data;
            result.type = 'ERROR';
        }
    } else {
        result.to = '';
        result.message = data;
        result.type = 'ERROR';
    }


    return result;
}


module.exports.getMessage = getMessage;
