function Message(to, from, subject, message, type, persona) {
    this._to = to;
    this._from = from;
    this._subject = subject;
    this._message = message;
    this._type = type;
    this._persona = persona;
}

Message.prototype = {

    //setters
    setTo: function(to) {
        this._to = to;
    },

    setFrom: function(name, uniqueName) {
        this._from = {};
        this._from.name = name;
        this._from.uniqueName = uniqueName;
    },

    setSubject: function(subject) {
        this._subject = subject;
    },

    setMessage: function(message) {
        this._message = message;
    },

    setType: function(type) {
        this._type = type;
    },

    //getters

    getTo: function() {
        return this._to;
    },


    getFrom: function() {
        return this._from;
    },

    getSubject: function() {
        return this._subject;
    },

    getMessage: function() {
        return this._message;
    },

    getType: function() {
        return this._type;
    }

};

module.exports = Message;
