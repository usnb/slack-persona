const main = require('../main.js');
const chai = require('chai');
var rewire = require('rewire');
const assert = chai.assert;

//use rewire to invoke the non-exported function 'parse'
const app = rewire('../main.js');
parse = app.__get__('parse');

describe('The parsing function: ', function() {
    it('parses the message', function() {
        var data = '#to={clara, john, tomas}\nHola a todos!\nSee you soon!\nRafael';
        var result = parse(data);

        assert.deepEqual(result.to, ['clara', 'john', 'tomas'])
        assert.equal(result.message, 'Hola a todos!\nSee you soon!\nRafael');

        data = 'Hola a todos!  #to={clara, john, tomas}\n\nSee you soon!\nRafael';
        result = parse(data);

        assert.deepEqual(result.to, ['clara', 'john', 'tomas'])
        assert.equal(result.message, 'Hola a todos!  \n\nSee you soon!\nRafael');

    });;

    it('produces the message object', function() {
        const data = '#to={ clara, john , tomas } \n Hola a todos!\nSee you soon!\nRafael   \n ';
        var message = main.getMessage(data);
        assert.deepEqual(message.getTo(), ['clara', 'john', 'tomas'])
        assert.equal(message.getMessage(), 'Hola a todos!\nSee you soon!\nRafael');
    });;

    it('catches an invalid message', function() {
        const data = ' \n Hola a todos!\nSee you soon!\nRafael   \n ';
        var message = main.getMessage(data);
        assert.equal(message.getType(), 'ERROR');
    })



})
