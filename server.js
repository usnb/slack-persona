//
//
//
'use strict';

var HashMap = require('hashmap');

// configs
var botconfig = require('./conf/bot.conf');
var serverconfig = require('./conf/server.conf');

var http = require('http');
var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
// PGR NOTE: here we have a variable that is a copy of a config info. Not necessary, can use serverconfig.port
// everywhere insteadd. Much cleaner to understand where the info is stored and avoids bad usage. Ahvar : done.
// Needed to send msg to amqp
//Ahvar : we moved sender up here.
var sender = require('./amqp-sender');

// They are needed to add interactive messages and buttons
var Message = require('message-transformer/message');


const slackManager = require('./managers/slackCommand');


// PGR NOTE: here we have a global variable used by an API method and rtm.
// this should be avoided as it will generate issues down the line.
// global variables should be avoided as much as possible.
var userState = new HashMap();

//
// Create and launch server
//

console.log('Create server and listen on port ' + serverconfig.port);
var app = express();
var urlencodedParser = bodyParser.urlencoded({extended: false});
http.createServer(app).listen(serverconfig.port);



// RealTimeMessaging 

const {RTMClient} = require('@slack/client');
// PGR NOTE: variable names should be long/meaningful. Here I ave no clue what rtm means.
// this is important for code readability

//
const rtm = new RTMClient(botconfig.token);
rtm.start();
var eventtext = '';
rtm.on('message', (event) => {
  // For structure of `event`, see https://api.slack.com/events/message

  if (eventtext != event.text && event.subtype != 'bot_message') {
    console.log(event);
    console.log(`(channel:${event.channel}) ${event.user} says: ${event.text}`);

    //    var timeOfPostback = event.timestamp;

    // PGR NOTE: beware bellow: javascript has = == and === as well as != !==
    // below should be !==
    if (userState.get(event.channel) !== undefined) {
      if (event.text !== undefined && event.text.toLowerCase() === "quit") {

        // PGR NOTE : below doesnt make sense. why build a message (which is entirely hardcoded)
        // and then only use one subpart. Bad coding should be fixed
//        var message = slackManager.generateButtonsHtml();
//        var attachments = message.attachments;
//        // message.text='You ended the conversation with ' + userState.get(event.channel) + '.';
//        web.chat.postMessage({
//          channel: event.channel,
//          "attachments": attachments
//        });
        slackManager.sendWebChatAttacheements(event.channel);

        userState.delete(event.channel);

      } else {

        //sendTextMessage(senderID, 'Message sent to ' + userState.get(senderID) + '!');
        var message = new Message();
        message._from = {
          name: event.user,
          uniqueName: event.channel
        };
        message._to = [userState.get(event.channel)];
        message._message = event.text; //should be taken from user
        message._type = 'TO';
        //            message._persona = confamqp.exchange.name;
        message._persona = 'slack'; // to inform bus that the messag is from slack

        sender.post(message, 'usnb');

        //       }
      }
    }
  }
  eventtext = event.text;
});


//Ahvar: we dont use 2 following functions, so we removed them
//sendMessageToSlack with out URL and channel ID , we dont use it here

/*

function sendMessageToSlackwithChannelID(JSONmessage, ChannelID) {
  var postOptions = {
    channel: ChannelID,
    uri: 'https://slack.com/api/chat.postMessage',
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    json: JSONmessage
  }
  request(postOptions, (error, response, body) => {
    if (error) {
      console.log(" problem :( " + error);

    }
  })
}

function sendMessageToSlack(JSONmessage) {
  var postOptions = {
    //uri: responseURL,
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    json: JSONmessage
  }
  request(postOptions, (error, response, body) => {
    if (error) {
      // handle errors as you see fit
    }
  })
}

*/



//
// REAT API methods for server
//

//
//
app.post('/slack/action', urlencodedParser, (req, res) => {
  res.status(200).end() // best practice to respond with 200 status
  var actionJSONPayload = JSON.parse(req.body.payload) // parse URL-encoded payload JSON string
  switch (actionJSONPayload.actions[0].name) {

    case "VisitPages":
      /*visit website should be added */
      break;
    case "MainMenu":
      // PGR NOTE what is the difference between this case and the slash-commands/send-me-buttons ??
	  //Ahvar : they are the same. In both case we will show the main menu
      // var message = MainMenu();
      // sendMessageToSlackResponseURL(actionJSONPayload.response_url, message)
      slackManager.processSendMeButtons(actionJSONPayload.response_url);
      break;

    case "About":
      // build about msg
      var message = new Message();
      message._from = {
        name: 'About',
        url: actionJSONPayload.response_url,
        channelid: actionJSONPayload.channel.id,
        uniqueName: actionJSONPayload.user.name
      };
      message._message = "About";
      message._type = "COMMAND";
      message._persona = 'slack';

      sender.post(message, 'usnb');      // send to RabbiMQ

      break;
    case "Users":
      // build users message
      var message = new Message();
      message._from = {
        name: 'ls',
        url: actionJSONPayload.response_url,
        channelid: actionJSONPayload.channel.id,
        uniqueName: actionJSONPayload.user.name
      };
      message._message = "ls";
      message._type = "COMMAND";
      message._persona = 'slack';

      sender.post(message, 'usnb'); // 'usnb' is exchange value which shows msg goes to bus
      break;
    case "users_list": // when users click on the menu

      var senderID = actionJSONPayload.channel.id;
      var recipientID = actionJSONPayload.actions[0].selected_options[0].value;

      //       console.log(actionJSONPayload.actions[0].selected_options[0].value);
      userState.set(senderID, recipientID);
      //sendStartSeparator(senderID, "This is a conversation with " + recipientID + ". Write \"quit\" to end it.");

      // PGR NOTE : Moved in slackCommad manager - message still build here but may make sense
      // to only send senderID and recipientID ?

      //web.chat.postMessage({
      //  channel: senderID,
      //  "attachments": [
      //    {
      //      "author_name": "",
      //      "image_url": "https://drive.google.com/open?id=1DE1CtGbBS363WMmFv-ib8Br66XL1OVEp",
      //      text: "This is a conversation with " + recipientID + ". Write \"quit\" to end it."
      //    }
      //  ]
      //})
      //  .then((res) => {
      //    // `res` contains information about the posted message
      //    console.log('Message sent: ', res.ts);
      //  })
      //  .catch(console.error);
      var msgToSend = {
        channel: senderID,
        "attachments": [
          {
            "author_name": "",
            "image_url": "https://drive.google.com/open?id=1DE1CtGbBS363WMmFv-ib8Br66XL1OVEp",
            text: "This is a conversation with " + recipientID + ". Write \"quit\" to end it."
          }
        ]
      };
      slackManager.webChatPostMessage(msgToSend);


      break;
  }
});

//
//
app.get('/ping', urlencodedParser, (req, res) => {
  // best practice to respond with 200 status
  res.status(200).end();
});

//
//
app.post('/slack/slash-commands/send-me-buttons', urlencodedParser, (req, res) => {
  var reqBody = req.body
  var responseURL = reqBody.response_url
  // test valid parameters

  // call slack manager
  slackManager.processSendMeButtons(responseURL);

  // best practice to respond with empty 200 status code
  res.status(200).end();
});

// PGR NOTE : why is this method exported by the server ? moved to stackManager
// module.exports.preparetosendMessageToSlack = preparetosendMessageToSlack;

