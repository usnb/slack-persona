//libraries
var assert = require('assert'), request = require('request');

// import our server into 'tests.js'
//var server = require('../server.js');

//start server before test
describe('server response', function () {
  before(function () {
  //  server.listen();
  });

it('should return 200', function (done) {
    // calling request with a GET and passing in the address of our server; in this case localhost listening on port 2876
    request.get('http://localhost:2876/ping', function (err, res, body){
    //function with error, response, and body callbacks. the function listening for a response 
    assert.equal(res.statusCode, 200);
    done();
  });
});
  
 
});